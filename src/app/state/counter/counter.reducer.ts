import {
  createFeatureSelector,
  createReducer,
  createSelector,
  on,
} from '@ngrx/store';
import {
  increment,
  decrement,
  reset,
  loadTodo,
  loadSuccess,
} from './counter.actions';

interface CounterState {
  counter: number;
  isloading: boolean;
}

export const initialState: CounterState = {
  counter: 0,
  isloading: false,
};

export const counterReducer = createReducer(
  initialState,
  on(increment, (state) => {
    return { ...state, counter: state.counter + 1 };
  }),
  on(decrement, (state) => {
    return { ...state, counter: state.counter - 1 };
  }),
  on(reset, () => initialState),
  on(loadTodo, (state) => {
    return { ...state, isloading: true };
  }),
  on(loadSuccess, (state, payload: any) => {
    return { ...state, todos: payload, isloading: false };
  })
);

// Create a feature selector to select the counter state
export const selectCounterState =
  createFeatureSelector<CounterState>('counter');

// Create a selector to get the count value from the counter state
export const counterState = createSelector(
  selectCounterState,
  (state: CounterState) => state.counter
);
