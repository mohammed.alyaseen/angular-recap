import { createAction, props } from '@ngrx/store';

export const increment = createAction('[Counter Component] Increment');
export const loadTodo = createAction('[Counter Component] loadTodo');
export const loadSuccess = createAction(
  '[Counter Component] loadSuccess',
  props<{ todos: Array<any> }>()
);
export const decrement = createAction('[Counter Component] Decrement');
export const reset = createAction('[Counter Component] Reset');
