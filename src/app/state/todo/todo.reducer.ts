import {
  createFeatureSelector,
  createReducer,
  createSelector,
  on,
} from '@ngrx/store';
import {
  reset,
  loadSuccess,
  loadStart,
  addTodo,
  deleteTodos,
} from './todo.actions';

interface TodoState {
  todos: Array<any>;
  isloading: boolean;
  lastIndex: number;
}

export const initialState: TodoState = {
  todos: [],
  isloading: false,
  lastIndex: 0,
};

export const todoReducer = createReducer(
  initialState,
  on(reset, () => initialState),
  on(loadStart, (state) => {
    return { ...state, isloading: true };
  }),
  on(loadSuccess, (state, payload: any) => {
    return { ...state, todos: payload.todos, isloading: false };
  }),
  on(addTodo, (state, payload: { todo: string }) => {
    console.log(payload);
    let lastIndex = state.lastIndex;
    if (lastIndex === 0 && state.todos.length !== 0) {
      lastIndex =
        state.todos.reduce((a, b) => (a.id > b.id ? a : b), {
          id: 0,
        }).id + 1;
    } else {
      lastIndex++;
    }
    return {
      ...state,
      todos: [
        {
          id: lastIndex,
          title: payload.todo,
        },
        ...state.todos,
      ],
      lastIndex,
    };
  }),
  on(deleteTodos, (state, payload: { ids: number[] }) => {
    let todos = [...state.todos];
    todos = todos.filter((todo) => !payload.ids.includes(todo.id));
    return { ...state, todos };
  })
);

// Create a feature selector to select the counter state
export const selectTodoState = createFeatureSelector<TodoState>('todo');

// Create a selector to get the count value from the counter state
export const todoState = createSelector(
  selectTodoState,
  (state: TodoState) => state
);
