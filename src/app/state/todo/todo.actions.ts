import { createAction, props } from '@ngrx/store';

export const loadStart = createAction('[Todo Component] Load Start');
export const loadSuccess = createAction(
  '[Todo Component] load success',
  props<{ todos: any }>()
);
export const loadFailed = createAction(
  '[Todo Component] load failed',
  props<{ todos: any }>()
);
export const reset = createAction('[Todo Component] Reset');
export const addTodo = createAction(
  '[Todo Component] Add Todo',
  props<{ todo: string }>()
);
export const deleteTodos = createAction(
  '[Todo Component] Delete Todos',
  props<{ ids: number[] }>()
);
