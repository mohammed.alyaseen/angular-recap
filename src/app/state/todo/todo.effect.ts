import { loadSuccess, loadStart, loadFailed } from './todo.actions';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, map } from 'rxjs/operators';
import { TodoService } from 'app/services/todo/todo.service';

@Injectable()
export class TodoEffects {
  loadMovies$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadStart),
      exhaustMap(() =>
        this.todoService.getTodos().pipe(
          map((todos) => loadSuccess({ todos })),
          catchError(() => of(loadFailed))
        )
      )
    )
  );

  constructor(private actions$: Actions, private todoService: TodoService) {}
}
