import { Component, Inject } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  addTodo,
  deleteTodos,
  loadStart,
  reset,
} from 'app/state/todo/todo.actions';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
} from '@angular/material/dialog';
import { MatListOption } from '@angular/material/list';
import { SelectionModel } from '@angular/cdk/collections';
import { MatSnackBar } from '@angular/material/snack-bar';

export interface TodoDialogProps {
  title: string;
}

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.less'],
})
export class TodoComponent {
  todos: Array<any> = [];
  title: string = '';
  // todos1$: Observable<any[]>;
  // List of selected todos keys .. Keys of the todos that are going to be deleted.
  selectedKeys: number[] = [];

  // TODO: apply type checking on store type
  // compiler didn't recognize the typo in reducer name "todos"
  constructor(
    private store: Store<{ todo: any }>,
    public dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {
    // this.todos1$ = this.store.pipe(select("todo"));
    console.log('todo-rendered-constructor');
  }

  ngOnInit() {
    this.store.dispatch(loadStart());
    this.store.select('todo').subscribe((state) => {
      console.log(state);
      const { todos } = state;
      // based on app requirements, slicing data should be inside the todo component its self or in the selector "todoState"
      this.todos = todos.slice(0, 20);
    });
    console.log('todo-rendered-ngOnInit');
  }

  refresh() {
    this.store.dispatch(loadStart());
  }

  reset() {
    this.store.dispatch(reset());
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(TodoDialogComponent, {
      data: { title: this.title },
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed');
      this.title = result;
      this.store.dispatch(addTodo({ todo: result }));
    });
  }

  onSelectionChange(options: SelectionModel<MatListOption>): void {
    const selectedKeys: number[] = [];
    options.selected.forEach((option) => {
      selectedKeys.push(option.value);
    });
    // List of selected todos keys .. Keys of the todos that are going to be deleted.
    this.selectedKeys = selectedKeys;
    // You can access the selected options here and perform any desired actions
  }

  onDelete() {
    if (this.selectedKeys.length > 0) {
      this.store.dispatch(deleteTodos({ ids: this.selectedKeys }));
      this.selectedKeys = [];
    } else
      this.snackBar.open(
        'Plzzz select some todos before click on delete',
        'warning'
      );
  }
}

@Component({
  selector: 'todo-dialog',
  templateUrl: 'todo-dialog.html',
  styleUrls: ['./todo.component.less'],
})
export class TodoDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<TodoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TodoDialogProps
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
