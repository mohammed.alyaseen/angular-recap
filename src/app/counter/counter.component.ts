import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as counterActions from 'app/state/counter/counter.actions';
import { counterState } from 'app/state/counter/counter.reducer';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.less'],
})
export class CounterComponent {
  counter$: Observable<any>;
  todos: any = [];

  constructor(private store: Store<{ counter: any }>) {
    // const { todos, counter } = this.store.pipe(select(counterState));
    // this.counter$ = counter
    this.counter$ = this.store.pipe(select(counterState));
  }

  increment() {
    this.store.dispatch(counterActions.increment());
  }

  decrement() {
    this.store.dispatch(counterActions.decrement());
  }

  reset() {
    this.store.dispatch(counterActions.reset());
  }
}
